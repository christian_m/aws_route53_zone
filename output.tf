output "soa_nameserver" {
  value = aws_route53_zone.domain.name_servers[3]
}

output "nameservers" {
  value = aws_route53_zone.domain.name_servers
}