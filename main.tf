resource "aws_route53_zone" "domain" {
  name = var.zone_name

  tags = {
    env = var.environment
  }
}

module "domain_ns" {
  source          = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  environment     = var.environment
  zone_id         = aws_route53_zone.domain.zone_id
  allow_overwrite = true
  record          = {
    name   = aws_route53_zone.domain.name
    type   = "NS"
    ttl    = "300"
    values = [
      aws_route53_zone.domain.name_servers.0,
      aws_route53_zone.domain.name_servers.1,
      aws_route53_zone.domain.name_servers.2,
      aws_route53_zone.domain.name_servers.3,
    ]
  }
}

module "domain_soa" {
  source          = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  environment     = var.environment
  zone_id         = aws_route53_zone.domain.zone_id
  allow_overwrite = true
  record          = {
    name   = aws_route53_zone.domain.name,
    type   = "SOA",
    ttl    = "300",
    values = [
      "${aws_route53_zone.domain.name_servers[3]}. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400",
    ],
  }
}

module "domain_records" {
  source   = "git::git@bitbucket.org:christian_m/aws_route53_resource_record.git?ref=v1.0"
  for_each = var.domain_records

  environment     = var.environment
  zone_id         = aws_route53_zone.domain.zone_id
  allow_overwrite = true
  record          = {
    name   = each.value.name
    type   = each.value.type
    ttl    = each.value.ttl
    values = each.value.values
  }
}